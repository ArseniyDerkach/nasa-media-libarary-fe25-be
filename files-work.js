import fs from 'fs';

// fs.readFile('./mock.json', "utf-8", (err, data) => {
//   console.log(data);
// })

// fs.writeFile('./mock.json', "lorem ipsum", function (err) {
//   if (err) {
//     throw err;
//   }
//   fs.readFile('./mock.json', "utf-8", (err, data) => {
//     console.log(data);
//   })
// })

// console.log(1);
// setTimeout(() => console.log(2));
// new Promise((resolve) => resolve()).then(() => console.log(3));
// console.log(4);

// fs.appendFile('./mock.json', JSON.stringify({ "id": 127, "firstName": "John", "lastName": "Doe", "position": "QA engineer" }), function (err) {
//   if (err) {
//     throw err;
//   }
// })

fs.writeFile('./data.txt', 'lorem ipsum', function (err) {
  if (err) {
    throw err;
  }
})

fs.appendFile('./data.txt', ' dolor sit amet.', function (err) {
  if (err) {
    throw err;
  }
})

fs.unlink('./data.txt', function (err) {
  if (err) {
    throw err;
  }
})