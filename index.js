// const userName = 'Arsenii'

// console.log('hello', userName);

// function strReverse(str) {
//   return str.split('').reverse().join('');
// }

// console.log(strReverse('hello'));

import express from 'express';
import cors from 'cors';
import jwt from 'jsonwebtoken';
import swaggerUi from 'swagger-ui-express';
import users from './users.js';
import { createNewUser, findUser, addToUserFavs, removeFromUserFavs } from './db/db.js';
import swaggerSpec from './swagger.js';
// create your own JWT secret with node -e "console.log(require('crypto').randomBytes(32).toString('hex'))"
const JWT_SECRET = '639fc3dd08dc65a13b42ce8fa0eac6d3171f9e196060f797a71f5b9c1ddc244d';


function myUserId(req, res, next) {
  if (Math.random() > 0.5) {
    next()
  } else {
    return res.status(404).send(JSON.stringify('Try again'));
  }
}

function verifyToken(req, res, next) {
  const authHeader = req.headers.authorization;
  console.log('authHeader', authHeader)
  if (!authHeader) {
    return res.status(401).json({ message: 'unauthorized' });
  }

  const token = authHeader.split(' ')[1];

  console.log('token', token);

  if (!token) {
    return res.status(401).json({ message: 'unauthorized' });
  }

  try {
    const decoded = jwt.verify(token, JWT_SECRET);
    console.log('decoded', decoded);
    req.user = decoded;
    if (Date.now() / 1000 > decoded.exp) {
      return res.status(401).json({ message: 'token has expired' })
    }
    next()
  } catch (err) {
    return res.status(401).json({ message: 'unauthorized' });
  }
}

const app = express();
app.use(cors());
app.use(express.json())

app.use('/api', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// app.use(function (req, res, next) {
//   console.log(req?.params);
//   if (req.path.indexOf('/user/') > -1) {
//     myUserId(req, res, next)
//   } else {
//     next()
//   }
// });


app.use(function (req, res, next) {
  if (req.path === '/auth' || req.path === '/signup') {
    next();
  } else {
    verifyToken(req, res, next);
  }
})


const port = 5174;

app.get('/', function (req, res) {
  res.send(JSON.stringify('hello from backend'));
})

app.post('/auth', async function (req, res) {
  const userToCheck = req.body;
  const foundUser = await findUser(userToCheck);
  if (!foundUser || foundUser?.password !== userToCheck.password) {
    return res.status(401).json({ message: 'Invalid username or password', token: '' });
  }
  const token = jwt.sign({ username: userToCheck.username }, JWT_SECRET);
  res.json({ message: 'logged in succesfully', token });
})

app.post('/signup', async function (req, res) {
  const newUser = req.body;
  // if (users.find(user => user.username === username)) {
  //   return res.status(400).json({ message: 'Username already exists' });
  // }
  // const newUser = { username, password };

  // users.push(newUser);

  // const token = jwt.sign({ username }, JWT_SECRET);

  const isUserFound = await findUser(newUser);

  if (!isUserFound) {
    const result = await createNewUser(newUser, JWT_SECRET);
    res.json(result);
  } else {
    res.status(400).json({ message: 'user already exists' });
  }
})

app.get('/user', function (req, res) {
  console.log('get user');
  res.json({ name: 'Arsenii' });
})

app.get('/user/:id', function (req, res) {
  const id = req.params.id;
  // console.log(users);
  const item = users.find(item => item.id == id);
  if (!item) res.status(404).send(JSON.stringify('Not found'));
  res.send(JSON.stringify(item));
})

app.get('/favs', async function (req, res) {
  const token = req.headers.authorization.split(' ')[1];
  const currentUser = jwt.verify(token, JWT_SECRET);

  const { favs } = await findUser(currentUser);

  res.json({ favs, message: 'received favs for current user' });
})

app.post('/addToFav', async function (req, res) {
  const { nasaId } = req.body;
  const token = req.headers.authorization.split(' ')[1];

  const { username } = jwt.verify(token, JWT_SECRET);

  await addToUserFavs(username, nasaId);

  res.json({ message: 'successfully added to fav!' });
})

app.post('/removeFromFav', async function (req, res) {
  const { nasaId } = req.body;
  const token = req.headers.authorization.split(' ')[1];

  const { username } = jwt.verify(token, JWT_SECRET);

  await removeFromUserFavs(username, nasaId);

  res.json({ message: 'successfully removed from fav!' });
})

// app.post('/user', function (req, res) {

//   // console.log(req.body);

//   const user = req.body;

//   users.push(user);

//   console.log(users);

//   // do smth

//   res.send(user);
// })


app.listen(port, () => {
  console.log(`server successfully started at port ${port}`);
})
