import { MongoClient, ServerApiVersion } from 'mongodb';
import jwt from 'jsonwebtoken';
const uri = "mongodb+srv://arseniiderkach:yQY1kkZ4GI5NAd42@cluster0.g8aio93.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

export async function createNewUser(user, JWT_SECRET) {
  try {
    await client.connect();

    const usersCollection = client.db('sample_mflix').collection('nasa_search_users');

    await usersCollection.insertOne({ ...user, favs: [] });

    const token = jwt.sign({ user: user.username }, JWT_SECRET);
    return {
      message: 'user created successfully!',
      token
    }
  } finally {
    await client.close();
  }
}

export async function findUser(user) {
  try {
    await client.connect();

    const usersCollection = client.db('sample_mflix').collection('nasa_search_users');

    const foundUser = await usersCollection.findOne({ username: user.username });

    if (foundUser) {
      return foundUser
    } else {
      return null
    }

  } finally {
    await client.close();
  }
}

export async function addToUserFavs(username, nasaId) {
  try {
    await client.connect();

    const usersCollection = client.db('sample_mflix').collection('nasa_search_users');

    const { favs } = await usersCollection.findOne({ username });

    const updatedFavs = { $set: { favs: [...favs, nasaId] } }

    await usersCollection.updateOne({ username }, updatedFavs);


  } finally {
    await client.close();
  }
}

export async function removeFromUserFavs(username, nasaId) {
  try {
    await client.connect();

    const usersCollection = client.db('sample_mflix').collection('nasa_search_users');

    const { favs } = await usersCollection.findOne({ username });

    const updatedFavs = { $set: { favs: [...favs.filter(fav => fav !== nasaId)] } }

    await usersCollection.updateOne({ username }, updatedFavs);


  } finally {
    await client.close();
  }
}


async function run() {
  try {
    await client.connect();

    const usersCollection = client.db('dafsdfsafsd').collection('greqgregreghewr');

    const user = await usersCollection.findOne({ username: 'Arsenii' });


    console.log(user);

    return {
      message: 'user created successfully!',
    }
  } finally {
    await client.close();
  }
}

// run();