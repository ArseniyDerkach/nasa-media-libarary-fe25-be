
import { MongoClient, ServerApiVersion } from 'mongodb';
const uri = "mongodb+srv://arseniiderkach:yQY1kkZ4GI5NAd42@cluster0.g8aio93.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

function autoincrement(collection, idx = null) {
  if (idx) {
    return ++idx
  }
  else {
    // find index in collection, ++index
  }
}

async function ping() {
  try {
    await client.connect();
    await client.db("admin").command({ ping: 1 });
    console.log("Pinged your deployment. You successfully connected to MongoDB!");
  } finally {
    await client.close();
  }
}
// ping().catch(console.dir);


async function showStarks() {
  try {
    await client.connect();
    const db = client.db('sample_mflix');

    const usersCollection = db.collection('users');

    // const allUsers = await usersCollection.find().toArray();

    const regex = new RegExp('Stark');

    // const allStarks = await usersCollection.find({ name: { $regex: regex } }).toArray();
    // const allStarks = await usersCollection.find({ name: { $regex: /Stark/ } }).toArray();


    // const nedStark = await usersCollection.find({ name: 'Ned Stark' }).toArray();

    // const uniqueNedStark = await usersCollection.findOne({ name: 'Ned Stark' });
    const firstStark = await usersCollection.findOne({ name: { $regex: regex } });

    console.log(firstStark);

  } finally {
    await client.close()
  }
}

async function createNewUsers() {
  try {
    await client.connect();
    const db = client.db('sample_mflix');

    const usersCollection = db.collection('new_users');
    const user = {
      name: 'John Doe',
      email: 'johndoe@gmail.com',
      password: '1234asdf'
    }
    const user2 = {
      name: 'Jane Doe',
      email: 'janedoe@gmail.com',
      password: '5678asdf'
    }
    await usersCollection.insertOne(user2);
    // await usersCollection.insertMany([user, user2]);
  } finally {
    await client.close()
  }
}

// createNewUsers();

async function updateUsers() {
  try {
    await client.connect();
    const db = client.db('sample_mflix');

    const usersCollection = db.collection('new_users');

    const filter = { name: 'Jane Doe' };
    const newJane = {
      name: 'Jane Daniels',
      email: 'janedaniels@yahoo.com',
      password: 'asdf1234'
    }
    const update = { $set: { password: '1234asdf' } }

    // const result = await usersCollection.updateOne(filter, update)

    // const result = await usersCollection.replaceOne(filter, newJane);
    // const something = await usersCollection.findOneAndReplace(filter, newJane);
    // await usersCollection.updateMany(filter, {$set: {origin: 'North'}})

    console.log(something);

  } finally {
    await client.close()
  }
}

// updateUsers();

async function run() {
  try {
    await client.connect();
    const db = client.db('sample_mflix');

    const usersCollection = db.collection('new_users');

    const filter = { name: 'Jane Daniels' };

    // const result = await usersCollection.deleteOne(filter);
    const result = await usersCollection.findOneAndDelete(filter);
    console.log(result);
  } finally {
    await client.close()
  }
}

// run();