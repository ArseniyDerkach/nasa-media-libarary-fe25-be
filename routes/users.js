/**
 * @swagger
 * /users:
 *    get: 
 *      summary: Get all users
 *      description: Retrieve a list of all users
 *      responces:
 *        200: 
 *          description: A list of users
 *          content:
 *            application/json
 */

router.get('/users', function (req, res) {
  // your route here
})

// yQY1kkZ4GI5NAd42