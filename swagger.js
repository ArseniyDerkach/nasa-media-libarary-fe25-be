import swaggerJSDoc from "swagger-jsdoc";

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Nasa media library authentification',
      version: '1.0.0',
      description: 'API documentation fro NASA media library authentification'
    },
    servers: [
      {
        url: 'http://localhost:5174'
      }
    ]
  },
  apis: ['./routes/*.js']
}

const swaggerSpec = swaggerJSDoc(options);

export default swaggerSpec;